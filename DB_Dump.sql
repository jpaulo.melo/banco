DROP TABLE IF EXISTS cedula CASCADE;

CREATE TABLE cedula
(    
    saldo integer NOT NULL,
    valor integer NOT NULL,
    img_path varchar(1000),
    CONSTRAINT cedula_pkey PRIMARY KEY (valor),
    CONSTRAINT cedula_valor_unique UNIQUE (valor)
)

DROP TABLE IF EXISTS role CASCADE;

CREATE TABLE role (
  role_id int NOT NULL ,
  role varchar(255) DEFAULT NULL,
  PRIMARY KEY (role_id)
);


INSERT INTO role (role_id, role)
VALUES
	(1,'ADMIN');


DROP TABLE IF EXISTS usuario CASCADE;

CREATE TABLE usuario (
  user_id int NOT NULL,
  active int DEFAULT NULL,
  email varchar(255) NOT NULL,  
  password varchar(255) NOT NULL,
  PRIMARY KEY (user_id)
);


INSERT INTO usuario (user_id, active, email, password)
VALUES
	(1,1,'nathanael.costa@casacivil.ce.gov.br','nathanael.costa'),
	(2,1,'jpaulo.melo','jpaulo.melo');

	
DROP TABLE IF EXISTS user_role;

CREATE TABLE user_role
(
    user_id integer NOT NULL,
    role_id integer NOT NULL,
    CONSTRAINT user_role_pk PRIMARY KEY (user_id, role_id),
    CONSTRAINT role_id_fk FOREIGN KEY (role_id)
        REFERENCES role (role_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT user_id_fk FOREIGN KEY (user_id)
        REFERENCES usuario (user_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);


INSERT INTO user_role (user_id, role_id)
VALUES
	(1,1);


