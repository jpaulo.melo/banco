package org.casacivil.banco;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.casacivil.banco.controller.RestCedulaController;
import org.casacivil.banco.modelo.Cedula;
import org.casacivil.banco.repository.CedulaRepository;
import org.casacivil.banco.service.CedulaService;
import org.casacivil.banco.vo.Saque;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(SpringJUnit4ClassRunner.class)
public class MovimentacaoBusiness2TiposDeCedulasTest {

	private MockMvc mockMvc;

	@Mock
	private CedulaRepository dataManager;

	@InjectMocks
	private CedulaService movimentacaoBusiness;
	
	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.standaloneSetup(movimentacaoBusiness).build();		
	}

	
	/**
	 * Testa o saque com dois tipos diferentes de cedulas para um saque de R$ 20,00
	 * 4 X R$ 5,00
	 * 2 X R$ 5,00 + 1 X R$ 10,00
	 * @throws Exception
	 */
	@Test
	public void opcoesDeSaqueDe20() throws Exception {
		Mockito.when(dataManager.findAll(new Sort(Sort.Direction.ASC, "valor"))).thenReturn(cedulasPossiveisPara20());	
		List<Saque> m = movimentacaoBusiness.opcoesDeSaque(20);
		
		// Dois saques são possiveis, 4 X R$ 5,00  // 2 X R$ 5,00 + 1 X R$ 10,00
		assertEquals(2, m.size());  
		
		// 4 X R$ 5,00
		assertEquals((Integer)4, (Integer)m.get(0).getMovimentacoes().get(0).getQuantidade()); 
		assertEquals((Integer)5, (Integer)m.get(0).getMovimentacoes().get(0).getCedula().getValor()); // 4 X R$ 5,00
		
		// 2 X R$ 5,00 + 1 X R$ 10,00
		assertEquals((Integer)2, (Integer)m.get(1).getMovimentacoes().get(0).getQuantidade()); // 2 X R$ 5,00 
		assertEquals((Integer)5, (Integer)m.get(1).getMovimentacoes().get(0).getCedula().getValor()); // 2 X R$ 5,00 
		
		assertEquals((Integer)1, (Integer)m.get(1).getMovimentacoes().get(1).getQuantidade()); // 1 X R$ 10,00
		assertEquals((Integer)10, (Integer)m.get(1).getMovimentacoes().get(1).getCedula().getValor()); // 1 X R$ 10,00
		
	}

		
	private ArrayList<Cedula> cedulasPossiveisPara20(){
		ArrayList<Cedula> cedulas = new ArrayList<Cedula>(); // Cedulas disponiveis ordenadas por valor

		Cedula c_10 = new Cedula(4, 5);
		Cedula c_20 = new Cedula(1, 10);
		Cedula c_50 = new Cedula(4, 50);
		Cedula c_100 = new Cedula(4, 100);

		cedulas.add(c_10);
		cedulas.add(c_20);
		cedulas.add(c_50);
		cedulas.add(c_100);
		
		return cedulas;
	}
	
	
}
