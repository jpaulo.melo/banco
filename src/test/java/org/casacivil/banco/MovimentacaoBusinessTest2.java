package org.casacivil.banco;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.casacivil.banco.modelo.Cedula;
import org.casacivil.banco.repository.CedulaRepository;
import org.casacivil.banco.service.CedulaService;
import org.casacivil.banco.vo.Saque;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(SpringJUnit4ClassRunner.class)
public class MovimentacaoBusinessTest2 {

	private MockMvc mockMvc;

	@Mock
	private CedulaRepository dataManager;

	@InjectMocks
	private CedulaService movimentacaoBusiness;
	
	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.standaloneSetup(movimentacaoBusiness).build();		
	}

	
	/**
	 * Em um saque de R$ 20, tendo 4 X R$ 5,00  e 1 X R$ 20,00
	 * as 4 X R$ 5,00 devem ser escolhidas para nivelar a quantidade notas
	 * no caixa.
	 * @throws Exception
	 */
	@Test
	public void melhorOpcaoDeSaqueTest() throws Exception {
		Mockito.when(dataManager.findAll(new Sort(Sort.Direction.ASC, "valor"))).thenReturn(cedulasDeTesteNiveisDesiguais());	
		Saque saque = movimentacaoBusiness.melhorOpcaoDeSaque(20);
		
		// Dois saques são possiveis, 4 X R$ 5,00  // 2 X R$ 5,00 + 1 X R$ 10,00
		assertNotNull(saque);
		
		// 4 X R$ 5,00
		assertEquals((Integer)4, (Integer)saque.getMovimentacoes().get(0).getQuantidade()); 
		assertEquals((Integer)5, (Integer)saque.getMovimentacoes().get(0).getCedula().getValor()); // 4 X R$ 5,00		
	}

	/**
	 * Como so possuimos R$ 40,00 em cedulas sera impossivel realiza um
	 *  saque maior que este valor.
	 * @throws Exception
	 */
	@Test
	public void saqueImpossivel() throws Exception {
		Mockito.when(dataManager.findAll(new Sort(Sort.Direction.ASC, "valor"))).thenReturn(cedulasDeTesteNiveisDesiguais());	
		Saque melhorOpcaoDeSaque = movimentacaoBusiness.melhorOpcaoDeSaque(45);	// Saque de R$ 45,00	
		assertEquals(melhorOpcaoDeSaque, null);	
		
		List<Saque> opcoesDeSaque = movimentacaoBusiness.opcoesDeSaque(45);	// Saque de R$ 45,00	
		assertEquals(opcoesDeSaque.size(), 0);		
	}
	
	private ArrayList<Cedula> cedulasDeTesteNiveisDesiguais(){
		ArrayList<Cedula> cedulas = new ArrayList<Cedula>(); // Cedulas disponiveis ordenadas por valor

		Cedula c_10 = new Cedula(4, 5);
		Cedula c_20 = new Cedula(1, 20);
		
		cedulas.add(c_10);
		cedulas.add(c_20);
		
		return cedulas;
	}
	
}
