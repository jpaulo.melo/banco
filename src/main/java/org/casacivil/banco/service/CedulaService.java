package org.casacivil.banco.service;

import java.util.ArrayList;
import java.util.List;

import org.casacivil.banco.modelo.Cedula;
import org.casacivil.banco.repository.CedulaRepository;
import org.casacivil.banco.vo.Movimentacao;
import org.casacivil.banco.vo.Saque;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class CedulaService {

	@Autowired
	private CedulaRepository movimentacaoDao;

	public Cedula salva(Cedula cedula) {
		return movimentacaoDao.save(cedula);
	}
	
	public Cedula pesquisa(Cedula cedula) {
		return movimentacaoDao.findOne(cedula.getValor());
	}
	

	public Saque melhorOpcaoDeSaque(Integer valor) {
		
		//ArrayList<Cedula> cedulas = new ArrayList<Cedula>(); // Cedulas disponiveis ordenadas por valor
		ArrayList<Cedula> cedulas = listaTodasCedulas();
						
		Saque SaqueOtima = null;
		Float notaOtima = Float.MAX_VALUE;
		Float notaOtimaAux = null;
		
		List<Saque> opcoesDeSaque = opcoesDeSaque(valor);
		for (Saque Saque : opcoesDeSaque) {
			Saque saque = (Saque)Saque;
			notaOtimaAux = saque.mediaAltura((ArrayList<Cedula>)cedulas.clone());
			if(notaOtimaAux < notaOtima) {
				SaqueOtima = Saque;
				notaOtima = notaOtimaAux;
			}
		}
		
		
		return (Saque)SaqueOtima;
	}

	/**
	 * Dado um valor lista os possiveis saques que podem ser realizados com as opções de de notas
	 *  em cada saque
	 * 
	 * @param valor
	 * @return
	 */
	public List<Saque> opcoesDeSaque(Integer valor) {
		List<Saque> opcoesDeSaque = new ArrayList<Saque>();
		ArrayList<Cedula> cedulas = listaTodasCedulas(); // Cedulas disponiveis ordenadas por valor
		
		// Valores com uma cedula
		for (Cedula C1 : cedulas) {
			for (int a = 1; a <= C1.getSaldo(); a++) {
				if (a * C1.getValor().intValue() == valor.intValue()) {					
					opcoesDeSaque.add(new Saque().add(new Movimentacao(C1, a)));
				}
			}
		}

		// Valores com duas cedula
		for (Cedula C1 : cedulas) {
			for (Cedula C2 : cedulas) {
				if (C1.getValor() < C2.getValor()) {
					for (int a = 1; a <= C1.getSaldo(); a++) {
						for (int b = 1; b <= C2.getSaldo(); b++) {
							if ((a * C1.getValor().intValue()) + (b * C2.getValor().intValue()) == valor.intValue()) {
								System.out.println(a + " X R$" + C1.getValor() + " + " + b + " X R$" + C2.getValor());
								opcoesDeSaque
										.add(new Saque().add(new Movimentacao(C1, a)).add(new Movimentacao(C2, b)));
							}
						}
					}
				}
			}
		}

		// Valores com tres cedula
		for (Cedula C1 : cedulas) {
			for (Cedula C2 : cedulas) {
				for (Cedula C3 : cedulas) {
					if ((C1.getValor() < C2.getValor()) && (C2.getValor() < C3.getValor())) { // evitar valores
																								// combinatorios

						for (int a = 1; a <= C1.getSaldo(); a++) {
							for (int b = 1; b <= C2.getSaldo(); b++) {
								for (int c = 1; c <= C3.getSaldo(); c++) {
									if ((a * C1.getValor().intValue()) + (b * C2.getValor().intValue())
											+ (c * C3.getValor().intValue()) == valor.intValue()) {
										System.out.println(a + " X R$" + C1.getValor() + " + " + b + " X R$"
												+ C2.getValor() + " + " + c + " X R$" + C3.getValor());
										opcoesDeSaque.add(new Saque().add(new Movimentacao(C1, a))
												.add(new Movimentacao(C2, b)).add(new Movimentacao(C3, c)));
									}

								}
							}
						}

					}
				}
			}
		}

		return opcoesDeSaque;
	}

	public List<Movimentacao> saqueEscolhido(List<Movimentacao> movimentacoes) {
		 
		for (Movimentacao movimentacao : movimentacoes) {
			Integer qtdASacar = movimentacao.getQuantidade();
			Cedula cedula = this.pesquisa(movimentacao.getCedula());			
			cedula.setSaldo(cedula.getSaldo() - qtdASacar);
			this.salva(cedula);
		}
		return null;
	}

	
	/**
	 * Lista as cedulas ordenadas pelo valor
	 * @return
	 */
	public ArrayList<Cedula> listaTodasCedulas() {
		ArrayList<Cedula> cedulas = new ArrayList<>();		
		for (Cedula c : movimentacaoDao.findAll(new Sort(Sort.Direction.ASC, "valor"))) {
			cedulas.add(c);
		}
		return cedulas;
	}
	
}
/**
 * Num. cedulas = 4 total de cedulas = 19
 * 
 * 10 20 50 100 MEDIA INDICE
 * 
 * 7 | 4 | 4 | 4
 * 
 * 7 | 0 | 4 | 4 5 4 5 | 1 | 4 | 4 3,5 5 3 | 2 | 4 | 4 3.25 3 1 | 3 | 4 | 4 3 4
 * 4 | 4 | 3 | 4 3,75 2 6 | 3 | 3 | 4 4 4
 **/
