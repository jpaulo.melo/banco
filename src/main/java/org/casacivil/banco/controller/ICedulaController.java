package org.casacivil.banco.controller;

import java.util.List;

import org.casacivil.banco.vo.Saque;

public interface ICedulaController {	
	public List<Saque> listOpcoesDeSaque(Integer valor);
	public List<Saque> melhorOpcaoSaque( Integer valor);
	public void saqueEscolhido(Integer item);	
}
