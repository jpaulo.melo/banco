package org.casacivil.banco.controller;

import org.casacivil.banco.modelo.Cedula;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class IndexController {

	@RequestMapping(value="/", method=RequestMethod.GET)
	public String index() {
		return "index";
	}
	
	@RequestMapping(value="/index", method=RequestMethod.GET)
	public String index(Cedula cedula) {		
		return "index";
	}
	
	@RequestMapping(value="cadastroCedula", method=RequestMethod.GET)
	public String cadastroCedula() {
		return "cedula/cadastroCedula";
	}
	
	@RequestMapping(value="cadastroSaque", method=RequestMethod.GET)
	public String cadastroSaque() {
		return "saque/cadastroSaque";
	}
}