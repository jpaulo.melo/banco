package org.casacivil.banco.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.casacivil.banco.modelo.Cedula;
import org.casacivil.banco.service.CedulaService;
import org.casacivil.banco.vo.Movimentacao;
import org.casacivil.banco.vo.Saque;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestCedulaController {

    @Autowired
    private CedulaService movimentacaoBusiness;

    private List<Saque> opcoesDeSaque = new ArrayList<Saque>();

    @RequestMapping(value = "/cadastroCedula",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public HttpEntity<Cedula> reabastecer(@Valid @RequestBody Cedula cedula) {
        return new ResponseEntity<>(movimentacaoBusiness.salva(cedula), HttpStatus.OK);
    }

    /**
     * Lista as opcoes de saque disponiveis entrado um valor a sacar.
     *
     * @param valor
     * @return
     */
    @RequestMapping(value = "/cadastroSaque",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<List<Saque>> listOpcoesDeSaque(@RequestBody Integer valor) {
        opcoesDeSaque = movimentacaoBusiness.opcoesDeSaque(valor);
        return new ResponseEntity<>(opcoesDeSaque, HttpStatus.OK);
    }

    /**
     * Retorna a melhor opcao de saque otimizado, nivelando a quantidade de
     * notas existentes em caixa
     *
     * @param valor
     * @return
     */
    @RequestMapping(value = "/melhorOpcaoSaque",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<List<Saque>> melhorOpcaoSaque(@RequestBody Integer valor) {
        Saque melhorOpcaoDeSaque = movimentacaoBusiness.melhorOpcaoDeSaque(valor);
               
        if (melhorOpcaoDeSaque != null) {
        	//melhorOpcaoDeSaque.getLabel();
            //opcoesDeSaque.add(melhorOpcaoDeSaque);
        	opcoesDeSaque.clear();
        	opcoesDeSaque.add(melhorOpcaoDeSaque);        	
        }
        return new ResponseEntity<>(opcoesDeSaque, HttpStatus.OK);
    }

    /**
     * Escolhe um saque a efetuar de uma lista de possibilidades de saques
     * possiveis retornados das funções listOpcoesDeSaque ou melhorOpcaoSaque
     *
     * @param item
     * @return 
     */
    @RequestMapping(value = "/cadastroSaqueEscolhido",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Saque> saqueEscolhido(@RequestBody Integer item) {
        final Saque saqueEscolhido = opcoesDeSaque.get(item);
        final List<Movimentacao> movimentacoes = saqueEscolhido.getMovimentacoes();
        movimentacaoBusiness.saqueEscolhido(movimentacoes);
        opcoesDeSaque = new ArrayList<Saque>();
        return new ResponseEntity<>(saqueEscolhido, HttpStatus.OK);
    }

    /**
     * Lista todas as cedulas ordenadas pelo campo valor
     *
     * @return
     */
    @RequestMapping(value = "/listaTodasCedulas",
            method = RequestMethod.GET,
            //consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Cedula>> listaTodasCedulas() {
        return new ResponseEntity<>(movimentacaoBusiness.listaTodasCedulas(), HttpStatus.OK);
    }

}
