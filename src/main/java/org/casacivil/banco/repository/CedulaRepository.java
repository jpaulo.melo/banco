package org.casacivil.banco.repository;

import org.casacivil.banco.modelo.Cedula;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CedulaRepository extends PagingAndSortingRepository<Cedula, Integer> {

}
