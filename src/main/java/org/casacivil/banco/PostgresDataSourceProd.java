package org.casacivil.banco;

import java.net.URISyntaxException;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

@Configuration
@Profile("prod")
public class PostgresDataSourceProd {

	@Bean
	//@Bean(destroyMethod = "close")
	public BasicDataSource dataSource() throws URISyntaxException {
		String dbUrl = "jdbc:postgresql://ec2-107-20-224-137.compute-1.amazonaws.com:5432/da2pim7tag5mqd";
		BasicDataSource basicDataSource = new BasicDataSource();
		basicDataSource.setUrl(dbUrl);
		basicDataSource.setUsername("jipwkocikopwjt");
		basicDataSource.setPassword("0c61d8058260b1c4759c3a687523e797d62643b6b90f5bf56632184fdf1f5c73");

		return basicDataSource;
	}

	@Bean
	public JpaVendorAdapter jpaVendorAdapter() {
		HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
		adapter.setDatabase(Database.POSTGRESQL);
		adapter.setShowSql(true);
		adapter.setGenerateDdl(true);
		adapter.setDatabasePlatform("org.hibernate.dialect.PostgreSQLDialect");
		adapter.setPrepareConnection(true);
		return adapter;
	}
}
