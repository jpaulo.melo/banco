package org.casacivil.banco.vo;

import org.casacivil.banco.modelo.Cedula;

/**
 * Quantidade a ser movimentada de uma cedula
 * 
 * @author desenv
 *
 */
public class Movimentacao {
	private Cedula cedula;
	private Integer quantidade;	
	
	public Movimentacao() {
		// TODO Auto-generated constructor stub
	}
	
	public Movimentacao(Cedula cedula, Integer valor) {
		this.cedula = cedula;
		this.quantidade = valor;
	}
	
	public Cedula getCedula() {
		return cedula;
	}
	public void setCedula(Cedula cedula) {
		this.cedula = cedula;
	}
	public Integer getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Integer valor) {
		this.quantidade = valor;
	}
		
}
