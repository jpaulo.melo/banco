package org.casacivil.banco.vo;

public class ResponseMessage {
	private String message;
	private MessageType type;
	
	public ResponseMessage() {
	    super();
	  }
	  
	  public ResponseMessage(String message, MessageType type) {
	    super();
	    this.message = message;
	    this.type = type;
	  }
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public MessageType getType() {
		return type;
	}
	public void setType(MessageType type) {
		this.type = type;
	}	
}
