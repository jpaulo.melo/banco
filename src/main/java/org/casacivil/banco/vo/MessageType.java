package org.casacivil.banco.vo;

public enum MessageType {
	SUCCESS, INFO, WARNING, ERROR;
}
