package org.casacivil.banco.vo;

import java.util.ArrayList;

import org.casacivil.banco.modelo.Cedula;

public class Saque {

	public String label;

	private ArrayList<Movimentacao> movimentacoes = new ArrayList<Movimentacao>();
	
	public Saque add(Movimentacao movimentacao) {
		movimentacoes.add(movimentacao);
		return this;
	}
	
	
	public ArrayList<Movimentacao> getMovimentacoes() {		
		return movimentacoes;
	}
	

	/**
	 * Valores calculados para apcoes de saque
	 * 
	 * @return
	 */
	public Float mediaAltura(ArrayList<Cedula> cedulas) {

		float indiceSaque = 0; // Valor que indica o peso da combinacao do saque
		float media = 0;
		float numeroDeCedulas = 0;
		float tiposDeCedulas = 0;

		// Conta numero de cedulas e tipos de cedulas que sobrou depois do saque
		// simulado
		for (Movimentacao movimentacao : getMovimentacoes()) {
			for (Cedula cedula : cedulas) {
				if (cedula.getValor().equals(movimentacao.getCedula().getValor())) {
					cedula.setSaldo(movimentacao.getCedula().getSaldo() - Math.abs(movimentacao.getQuantidade()));
				}
			}
		}

		for (Cedula cedula : cedulas) {
			if (cedula.getSaldo() > 0) {
				numeroDeCedulas += cedula.getSaldo();
				tiposDeCedulas += 1;
			}
		}

		media = numeroDeCedulas / tiposDeCedulas;

		for (Cedula cedula : cedulas) {
			if (cedula.getSaldo().intValue() != 0) {
				indiceSaque += Math.abs(cedula.getSaldo().floatValue() - media); // Melhor distribuicao para manter
																					// maior proporcao de cedulas
			}
		}

		return indiceSaque;
	}

	@Override
	public String toString() {
		return getLabel();
	}

	public String getLabel() {
		StringBuffer stringBuffer = new StringBuffer();

		for (Movimentacao movimentacao : getMovimentacoes()) {
			stringBuffer.append(movimentacao.getQuantidade() + " X R$" + movimentacao.getCedula().getValor() + " + ");
		}

		if (stringBuffer.length() > 0) {
			stringBuffer.delete(stringBuffer.length() - 2, stringBuffer.length());
		}
		setLabel(stringBuffer.toString());
		return stringBuffer.toString();
	}

	public void setLabel(String label) {
		this.label = label;
	}

}
