package org.casacivil.banco.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
public class Cedula implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(unique = true)
	@NotNull(message = "error.valor.notnull")
	@Min(value=2, message = "error.valor.maior")
	//@Pattern(regexp="[2|5|10|20|50|100]", message="error.valor.pattern")	
	private Integer valor;

	@Column
	@NotNull(message = "error.saldo.notnull")	
	@Max(value=100, message = "error.saldo.menor")
	@Min(value=0, message = "error.saldo.maior")
	private Integer saldo;

//	@Column
//	private String imgPath;

	public Cedula() {
		// TODO Auto-generated constructor stub
	}

	public Cedula(Integer qtd, Integer valor) {
		this.saldo = qtd;
		this.valor = valor;
	}

	public Integer getSaldo() {
		return saldo;
	}

	public void setSaldo(Integer qtd) {
		this.saldo = qtd;
	}

	public Integer getValor() {
		return valor;
	}

	public void setValor(Integer valor) {
		this.valor = valor;
	}

//	public String getImgPath() {
//		return imgPath;
//	}
//
//	public void setImgPath(String imgPath) {
//		this.imgPath = imgPath;
//	}

}
