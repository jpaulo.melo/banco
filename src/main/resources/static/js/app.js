var appCaixa = angular.module("appCaixa", []);

appCaixa.controller("saque", function($scope, $http, $timeout) {
	$scope.aSacar = 0;
	$scope.opcaoSaqueEscolhido = 0;
	$scope.tiposSaque = '';

	$scope.limpar = function() {
		$scope.tiposSaque = '';
	};
	
	 
    // Lista as opcoes de saque disponiveis entrado um valor a sacar.	 
	$scope.carregaOpcoesSaque = function() {
		$http({
			method : 'POST',
			url : '/cadastroSaque',
			data : $scope.aSacar
		}).then(function successCallback(response) {			
			$scope.tiposSaque = response.data;
			if($scope.tiposSaque == ""){				
				document.getElementById("panel-saque").className = "collapse in";
				document.getElementById("radios-saque").className = "collapse";				
				document.getElementById("botoes-saque").className = "collapse";				
				
				$scope.errorMessage = "O valor não é possivel ser sacado, escolha outro.";
				$scope.displayError = true;
		        $timeout(function () {
		             $scope.displayError = false;
		             document.getElementById("radios-saque").className = "collapse";
		        }, 5000);
			}
			
		}, function errorCallback(response) {
			document.getElementById("panel-saque").className = "collapse in";
			document.getElementById("radios-saque").className = "collapse";				
			document.getElementById("botoes-saque").className = "collapse";				
			
			$scope.errorMessage = "O valor não é possivel ser sacado, escolha outro.";
			$scope.displayError = true;
	        $timeout(function () {
	             $scope.displayError = false;
	             document.getElementById("radios-saque").className = "collapse";
	        }, 5000);
			console.log(response.status);
			console.log(response.data);
		});
	};
	
	// Retorna a melhor opcao de saque otimizado, nivelando a quantidade de notas existentes em caixa
	$scope.melhorOpcaoSaque = function() {
		$http({
			method : 'POST',
			url : '/melhorOpcaoSaque',
			data : $scope.aSacar
		}).then(function successCallback(response) {
			$scope.tiposSaque = response.data;
			if($scope.tiposSaque == ""){
				//alert("Escolha outro valor de saque !"); 
				//$scope.limpar();
				document.getElementById("panel-saque").className = "collapse in";
				document.getElementById("radios-saque").className = "collapse";				
				document.getElementById("botoes-saque").className = "collapse";				
				
				$scope.errorMessage = "Valores possíveis de saque devem ser multiplos de R$ 5";
				$scope.displayError = true;
		        $timeout(function () {
		             $scope.displayError = false;
		             document.getElementById("radios-saque").className = "collapse";
		        }, 5000);
            }
		}, function errorCallback(response) {
			console.log(response.status);
			console.log(response.data);
		});
	};
	
	//Escolhe um saque a efetuar de uma lista de possibilidades de saques
	// possiveis retornados das funções listOpcoesDeSaque ou melhorOpcaoSaque
	$scope.saqueEscolhido = function() {
		console.log($scope.opcaoSaqueEscolhido);
		$http({
			method : 'POST',
			url : '/cadastroSaqueEscolhido',
			data : $scope.opcaoSaqueEscolhido
		}).then(function successCallback(response) {
			$scope.tiposSaque = '';
			$scope.aSacar = 0;
			$scope.successMessage = "Saque cadastrado com sucesso.";
	        $scope.displaySucess = true;
	        $timeout(function () {
	             $scope.displaySucess = false;	             
	        }, 5000);
			
		}, function errorCallback(response) {			
			$scope.tiposSaque = '';
			$scope.aSacar = 0;
			
			$scope.errorMessage = response.data.message;
			$scope.displayError = true;
	        $timeout(function () {
	             $scope.displayError = false;	             
	        }, 5000);
	        
			console.log(response.status);
			console.log(response.data);
		});
	};
	
	

});



// CONTROLLER DE CEDULAS //

appCaixa.controller("cedula", function($scope, $http, $timeout) {
	$scope.cedula = {"valor":"", "saldo":""};
	$scope.cedulasPesquisa;

	//Lista todas as cedulas ordenadas pelo campo valor
	$scope.listaTodasCedulas = function() {
		$http({
			method : 'GET',
			url : '/listaTodasCedulas',
			headers: { 'Content-Type': 'application/json' }
		}).then(function successCallback(response) {
			$scope.cedulasPesquisa = response.data;
			console.log($scope.cedulasPesquisa);
		}, function errorCallback(response) {
			console.log(response.status);
			console.log(response.data);
		});
	};
	
	// Salva a cedula
	$scope.editaCedula = function(cedulaEdit) {
		
		$scope.cedula = angular.copy(cedulaEdit);
		$http({
			method : 'POST',
			url : '/cadastroCedula',
			data : $scope.cedula
		}).then(function successCallback(response) {			
			
		}, function errorCallback(response) {
			$scope.displaySucess = false;
			console.log(response.status);
			console.log(response.data);
		});
	};
	
	// Salva a cedula
	$scope.salvaCedula = function() {
		
		$http({
			method : 'POST',
			url : '/cadastroCedula',
			data : $scope.cedula
		}).then(function successCallback(response) {			
			$scope.cedula = {};
			$scope.successMessage = "Cédula cadastrada com sucesso";
	        $scope.displaySucess = true;
	        $timeout(function () {
	             $scope.displaySucess = false;	             
	        }, 5000);
		}, function errorCallback(response) {						
			$scope.errorMessage = response.data.message;
			$scope.displayError = true;
	        $timeout(function () {
	             $scope.displayError = false;	             
	        }, 5000);
			
			console.log(response.data.message);
			console.log(response.data.type);			
			console.log(response.status);			
		});
	};
});