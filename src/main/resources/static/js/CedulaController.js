appCaixa.controller("saque", function($scope, $http) {
	$scope.aSacar = 0;
	$scope.opcaoSaqueEscolhido = 0;

	$scope.carregaOpcoesSaque = function() {
		$http({
			method : 'POST',
			url : 'http://localhost:8080/cadastroSaque',
			data : $scope.aSacar
		}).then(function successCallback(response) {
			$scope.tiposSaque = response.data;
			console.log($scope.tiposSaque);
		}, function errorCallback(response) {
			console.log(response.status);
			console.log(response.data);
		});
	};
	
	
	$scope.melhorOpcaoSaque = function() {
		$http({
			method : 'POST',
			url : 'http://localhost:8080/melhorOpcaoSaque',
			data : $scope.aSacar
		}).then(function successCallback(response) {
			$scope.tiposSaque = response.data;
			console.log($scope.tiposSaque);
		}, function errorCallback(response) {
			console.log(response.status);
			console.log(response.data);
		});
	};
	
	
	$scope.saqueEscolhido = function() {
		console.log($scope.opcaoSaqueEscolhido);
		alert($scope.opcaoSaqueEscolhido);
		$http({
			method : 'POST',
			url : 'http://localhost:8080/cadastroSaqueEscolhido',
			data : $scope.opcaoSaqueEscolhido
		}).then(function successCallback(response) {
			alert(response.data);
		}, function errorCallback(response) {
			console.log(response.status);
			console.log(response.data);
		});
	};

});